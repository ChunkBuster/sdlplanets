#pragma once
#include <stdlib.h>
#include <string>
#include <SDL.h>

const extern int SCREEN_WIDTH;
const extern int SCREEN_HEIGHT;

//The window we'll be rendering to 
extern SDL_Window* gWindow;
//The window renderer
extern SDL_Renderer* gRenderer;
//The surface contained by the window 
extern SDL_Surface* gScreenSurface;
//The image we will load and show on the screen 
extern SDL_Surface* gHelloWorld;
//Loads individual image
extern SDL_Surface* LoadSurface(std::string path);
//Current displayed image
extern SDL_Surface* gStretchedSurface;


int pixel(SDL_Renderer *renderer, Sint16 x, Sint16 y);
int DrawCircle(SDL_Renderer * renderer, int x0, int y0, Uint16 radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a);