﻿using namespace std;
#include <Display.h>

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

//The window we'll be rendering to 
SDL_Window* gWindow = NULL;
//The window renderer
SDL_Renderer* gRenderer = NULL;
//The surface contained by the window 
SDL_Surface* gScreenSurface = NULL;
//The image we will load and show on the screen 
SDL_Surface* gHelloWorld = NULL;
//Loads individual image
SDL_Surface* LoadSurface(std::string path);
//Current displayed image
SDL_Surface* gStretchedSurface = NULL;



int pixel(SDL_Renderer *renderer, Sint16 x, Sint16 y)
{
	return SDL_RenderDrawPoint(renderer, x, y);
}

int DrawCircle(SDL_Renderer * renderer, int x0, int y0, Uint16 radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	int result;

	int x = radius;
	int y = 0;
	int err = 0;

	SDL_SetRenderDrawColor(gRenderer, r, g, b, a);

	while (x >= y)
	{
		result |= pixel(renderer, x0 + x, y0 + y);
		result |= pixel(renderer, x0 + y, y0 + x);
		result |= pixel(renderer, x0 - y, y0 + x);
		result |= pixel(renderer, x0 - x, y0 + y);
		result |= pixel(renderer, x0 - x, y0 - y);
		result |= pixel(renderer, x0 - y, y0 - x);
		result |= pixel(renderer, x0 + y, y0 - x);
		result |= pixel(renderer, x0 + x, y0 - y);

		y += 1;
		if (err <= 0)
		{
			err += 2 * y + 1;
		}
		if (err > 0)
		{
			x -= 1;
			err -= 2 * x + 1;
		}
	}
	return (result);
}
