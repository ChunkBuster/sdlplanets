#pragma once
#include <Planet.h>
#include <chrono>
#include <thread>
#include <SDL.h>
#include <Display.h>

bool Init();
bool LoadMedia();
void Close();
void RenderFrame(Planet *planets[], int number);


bool Init() {


	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return false;
	}
	else {
		//Create window 
		gWindow = SDL_CreateWindow("SDL Stars", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return false;
		}
		//Get window surface 
		gScreenSurface = SDL_GetWindowSurface(gWindow);
		//Create renderer for window
		gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
		if (gRenderer == NULL)
		{
			printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
			return false;
		}
	}
	
	return true;
}

bool LoadMedia() {
	//Loading success flag
	bool success = true;
	//Load stretching surface
	gStretchedSurface = LoadSurface("stretch.bmp");
	if (gStretchedSurface == NULL)
	{
		printf("Failed to load stretching image!\n");
		success = false;
	}

	return success;
}


SDL_Surface* LoadSurface(std::string path)
{
	//The final optimized image
	SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = SDL_LoadBMP(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
	}
	else
	{
		//Convert surface to screen format
		optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}
	
	return optimizedSurface;
}

void Close() {
	//Deallocate surface 
	SDL_FreeSurface(gHelloWorld);
	gHelloWorld = NULL;
	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	//Destroy renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;
	//Quit SDL subsystems 
	SDL_Quit();
}

