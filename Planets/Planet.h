#pragma once
#include <stdlib.h>
#include <iostream>  
#include <iomanip> 
#include <string>
#include <ctime>
#include <cstdio>
#include <StringFormat.h>
#include <Vector.h>
#include <PhysicalObject.h>
#include <Quadtree.h>

class Planet : public PhysicalObject
{
public:
	std::string Name;
	float Size;
	int color[3] = { (rand() % 255), (rand() % 255), (rand() % 255) };
	//Constructor
	Planet(std::string n, float mass, Vector2* position);
	//Destructor
	~Planet();
	//Functions
	std::string ToString();
	void Update();
	
	static Planet *GetRandomPlanet();
};



