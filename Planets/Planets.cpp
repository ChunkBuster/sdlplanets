using namespace std;
#include<Planets.h>

int main(int argc, char ** argv)
{
	//Seed the random
	srand(time(0));
	
	//Load the window and media
	if (!Init()) { 
		cout << ("Failed to initialize!\n");
		Close();
		getchar();
		return 1;
	}

	SDL_SetWindowFullscreen(gWindow, 0);

	//Init physics
	PhysicalObject::Bounds->x = SCREEN_WIDTH;
	PhysicalObject::Bounds->y = SCREEN_HEIGHT;
	
	Quadtree* tree = new Quadtree();


	Planet* planets[200];
	for (int i = 0; i < sizeof(planets) / sizeof(planets[0]); i++) {
		
		if (i == 0) {
			planets[i] = new Planet("Sun", 10e6, new Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
			planets[i]->Static = true;
			continue;
		}

		planets[i] = Planet::GetRandomPlanet();
		tree->InsertData(planets[i], planets[i]->Position);
	}

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	//While application is running, loop
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
		}

		//Clear screen
		//SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_SetRenderDrawColor(gRenderer, 1, 1, 1, 1);
		SDL_RenderClear(gRenderer);

		//Apply the image stretched
		SDL_Rect stretchRect;
		stretchRect.x = 0;
		stretchRect.y = 0;
		stretchRect.w = SCREEN_WIDTH;
		stretchRect.h = SCREEN_HEIGHT;
		SDL_BlitScaled(gStretchedSurface, NULL, gScreenSurface, &stretchRect);

		
		//Draw planets
		//RenderFrame(planets, sizeof(planets) / sizeof(planets[0]));

		//Draw Quadtree
		tree->Update();
		delete(tree);
		tree = new Quadtree();
		for (int i = 0; i < sizeof(planets) / sizeof(planets[0]); i++) {

			if (i == 0) {
				planets[i] = new Planet("Sun", 10e6, new Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
				planets[i]->Static = true;
				continue;
			}

			planets[i] = Planet::GetRandomPlanet();
			tree->InsertData(planets[i], planets[i]->Position);
		}

		//Update screen
		SDL_RenderPresent(gRenderer);
	}

	Close();
	
	return 0;
}

void RenderFrame(Planet *planets[], const int number) {
	//SDL_Rect** fillRects = new SDL_Rect*[number];
	
	for (int i = 0; i < number; i++) {
		planets[i]->Update();

		for (int u = 0; u < number; u++) {
			if (i == u)
				continue;



			//Force acting on planet
			double force = planets[u]->GravitationalForce(*planets[i]);
			//force = 0;
			//Ignore infinite force
			if (force == INFINITY || force == -INFINITY)
				continue;

			//Direction of the other planet
			Vector2 dir = *(*planets[u]).Position - *(*planets[i]).Position;
			dir.Normalize();

			(*planets[i]).Velocity->x += (dir.x) * (force);
			(*planets[i]).Velocity->y += (dir.y) * (force);

				

			//If planets are too close, destroy and continue
			if (Vector2::Distance(*(*planets[i]).Position, *(*planets[u]).Position) < planets[i]->Size + planets[u]->Size) {
				if ((*planets[i]).Mass > (*planets[u]).Mass) {
					
					planets[i]->Mass += planets[u]->Mass;

					planets[i]->color[0] = ((double)(
					(double)planets[i]->color[0] * (double)planets[i]->Mass) +
					((double)planets[u]->color[0] * (double)planets[u]->Mass)) /
					((double)planets[i]->Mass + (double)planets[u]->Mass);

					planets[i]->color[1] = ((double)(
					(double)planets[i]->color[1] * (double)planets[i]->Mass) +
					((double)planets[u]->color[1] * (double)planets[u]->Mass)) /
					((double)planets[i]->Mass + (double)planets[u]->Mass);

					planets[i]->color[2] = ((double)(
					(double)planets[i]->color[2] * (double)planets[i]->Mass) +
					((double)planets[u]->color[2] * (double)planets[u]->Mass)) /
					((double)planets[i]->Mass + (double)planets[u]->Mass);



					delete planets[u];
					planets[u] = Planet::GetRandomPlanet();
					//cout << planets[i]->Name << " destroyed " << planets[u]->Name << ". " << planets[i]->Name << " now has a mass of: " << planets[i]->Mass << " kg." << '\n';
				}
			}

		}
		
		//Size and position for drawing
		int x = int(planets[i]->Position->x);
		int y = int(planets[i]->Position->y);
		float size = float(planets[i]->Size);

		DrawCircle(gRenderer, x, y, size, planets[i]->color[0], planets[i]->color[1], planets[i]->color[2], 1);
		pixel(gRenderer, planets[i]->Position->x, planets[i]->Position->y);
	}
}

