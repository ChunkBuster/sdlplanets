#pragma once
#include <stdlib.h>
#include <iostream>  
#include <iomanip> 
#include <string>
#include <ctime>
#include <cstdio>
#include <Vector.h>
#include <SKSMath.h>
#include <Display.h>

class Node {};
class Quadtree {
	//Node def
	class Node {

	private:
		Node* Nodes[4];
		void* Data;

	public:
		Node(Vector2* position, int level, bool base);
		~Node();

		int Level;
		bool Base;
		Vector2* Position;

		void* Get(Vector2* position);
		void Insert(void* data, int level, Vector2* position);
		void Update();
	};

	//Quadtree internals
private:
	Node* Master;
public:
	Quadtree();
	~Quadtree();

	void* GetData(Vector2* position);
	void InsertData(void* data, Vector2* position);
	Node* Nodes[4];
	void Update();
};
