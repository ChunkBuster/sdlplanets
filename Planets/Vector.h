#pragma once
#include <stdlib.h>
#include <iostream>  
#include <iomanip> 
#include <string>
#include <ctime>
#include <cstdio>
#include <StringFormat.h>

class Vector2 {

public:
	double x;
	double y;

	//Constructors
	Vector2(double x, double y);
	Vector2();

	//Destructor
	~Vector2();

	//Static block
	static double Distance(Vector2 A, Vector2 B);

	//Functions
	std::string ToString();
	Vector2* Normalize();

	//Mathematical operations with other vectors

	//Addition
	Vector2&operator+(const Vector2& other)
	{
		float newX = x + other.x;
		float newY = y + other.y;
		return Vector2(newX, newY);
	}

	//Subtraction
	Vector2&operator-(const Vector2& other)
	{
		float newX = x - other.x;
		float newY = y - other.y;
		return Vector2(newX, newY);
	}

	//inplace addition
	Vector2&operator+=(const Vector2& other)
	{
		x += other.x;
		y += other.y;
		return *this;
	}

	//inplace subtraction
	Vector2&operator-=(const Vector2& other)
	{
		x -= other.x;
		y -= other.y;
		return *this;
	}

	//Mathematical operations with scalars
	
	//Addition
	Vector2&operator+(const double& other)
	{
		float newX = x + other;
		float newY = y + other;
		return Vector2(newX, newY);
	}

	//Subtraction
	Vector2&operator-(const double& other)
	{
		float newX = x + other;
		float newY = y + other;
		return Vector2(newX, newY);
	}

	//Multiplication
	Vector2&operator*(const double& other)
	{
		float newX = x * other;
		float newY = y * other;
		return Vector2(newX, newY);
	}

	//Division
	Vector2&operator/(const double& other)
	{
		float newX = x / other;
		float newY = y / other;
		return Vector2(newX, newY);
	}

	//inplace subtraction
	Vector2&operator-=(const double& other)
	{
		x += other;
		y += other;
		return *this;
	}

	//inplace multiplication
	Vector2&operator*=(const double& other)
	{
		x *= other;
		y *= other;
		return *this;
	}

	//inplace division
	Vector2&operator/=(double other)
	{
		x /= other;
		y /= other;
		return *this;
	}
};


