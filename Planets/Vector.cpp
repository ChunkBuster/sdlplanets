using namespace std;
#include<Vector.h>

//Physical positioning structs

	//Constructors
	Vector2::Vector2() {
		x = 0;
		y = 0;
	}

	Vector2::Vector2(double xPos, double yPos) {
		x = xPos;
		y = yPos;
	}	

	//Destructor
	Vector2::~Vector2() {

	}

	//Static methods
	double Vector2::Distance(Vector2 A, Vector2 B) {
		return sqrt(pow(A.x - B.x, 2) + pow(A.y - B.y, 2));
	}

	
	string Vector2::ToString() {
		return string_format("(%e,%e)", x, y);
	}

	//Returns a normalized version of the vector
	Vector2* Vector2::Normalize() {
		double length = Vector2::Distance(Vector2(0, 0), *this);
		
		if (length != 0) {
			x /= length;
			y /= length;
		}

		return this;
	}

