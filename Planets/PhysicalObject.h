#pragma once
#include <stdlib.h>
#include <string>
#include <iostream>  
#include <iomanip> 
#include <string>
#include <ctime>
#include <cstdio>
#include <Vector.h>
#include <StringFormat.h>


class PhysicalObject {
public:
	double Mass;
	Vector2* Position;
	Vector2* Velocity;
	static Vector2* Bounds;

	bool Static;

	//Constructors
	PhysicalObject(double mass);
	PhysicalObject(double mass, Vector2* position);

	//Destructor
	~PhysicalObject();

	//Functions
	double GravitationalForce(PhysicalObject& other);
	std::string ToString();
	void Update();
	void AddForce(double force, Vector2* dir);
};


