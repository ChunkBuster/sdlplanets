using namespace std;
#include<Planet.h>


//Planet
Planet::Planet(string n, float mass, Vector2* position)
	: PhysicalObject(mass, position) {
	Name = n;	
}

//Unplanet
Planet::~Planet() {
	
}


string Planet::ToString() {
	return ("\nName: " + Name + '\n' + PhysicalObject::ToString() + '\n');
}

void Planet::Update() {
	Size = sqrt(Mass / 3.14159f) / 10;
	Size = Size > 2 ? Size : 2;
	PhysicalObject::Update();
}
//Random Planet Generation

Planet *Planet::GetRandomPlanet() {
	string names[] = {
		"Togleaphus", "Zocroistea",
		"Gubruna", "Cudrorix",
		"Zagantu", "Banov",
		"Swenelia", "Stralurus",
		"Tebliuvis","Fecreuturn",
		"Kostrorix", "Oskyria",
		"Xeynia", "Beyhines",
		"Gluhanides","Glafetune" };

	string name = names[rand() % sizeof(names) / sizeof(names[0])];

	//double mass = ((rand() % 20) + 10) * pow(10, (rand() % 4) + 25);	
	double mass = (rand() % 10) + 1 * pow(10, (rand() % 5) + 0.5);
	Vector2* position;
	position = new Vector2(rand() % (int)(PhysicalObject::Bounds->x), rand() % (int)(PhysicalObject::Bounds->y));
	Planet* newPlanet = new Planet(name, mass, position);
	newPlanet->Velocity = new Vector2(((float)(rand() % 10) - 5.0f) / 10.0f, ((float)(rand() % 10) - 5.0f) / 10.0f);
	return newPlanet;
}

