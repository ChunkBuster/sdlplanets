#include <Quadtree.h>

//Constructors
Quadtree::Quadtree() {
	Master = new Node(new Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2), 1, true);
}

Quadtree::Node::Node(Vector2* position, int level, bool base) {
	Position = position;
	Base = true;
	Level = level;
	Data = NULL;
}

//Destructors
Quadtree::~Quadtree() {
	delete(Master);
}

Quadtree::Node::~Node() {
	if (Base) return;
	for (int i = 0; i < 4; i++) {
		delete(Nodes[i]);
	}
}

//Getter
void* Quadtree::GetData(Vector2* position) {
	return Master -> Get(position);
}

void* Quadtree::Node::Get(Vector2* position) {

}

//Insert
void Quadtree::InsertData(void* data, Vector2* position) {
	Master->Insert(data, Master->Level, position);
}

void Quadtree::Node::Insert(void* data, int level, Vector2* position) {
	void* ass(this);
	if (Base) {
		Data = data;
		Base = false;

		float xMod = ((SCREEN_WIDTH / pow(2, level + 2)));
		float yMod = ((SCREEN_HEIGHT / pow(2, level + 2)));
		//float xMod = 0;
		//float yMod = 0;
		
		Nodes[0] = new Node(new Vector2(Position->x + xMod, Position->y + yMod), level + 1, true);
		Nodes[1] = new Node(new Vector2(Position->x - xMod, Position->y + yMod), level + 1, true);
		Nodes[2] = new Node(new Vector2(Position->x - xMod, Position->y - yMod), level + 1, true);
		Nodes[3] = new Node(new Vector2(Position->x + xMod, Position->y - yMod), level + 1, true);
		/*
		Nodes[0] = new Node(new Vector2(0, 0), level + 1, true);
		Nodes[1] = new Node(new Vector2(0, 0), level + 1, true);
		Nodes[2] = new Node(new Vector2(0, 0), level + 1, true);
		Nodes[3] = new Node(new Vector2(0, 0), level + 1, true);*/
		return;
	}

	bool left;
	bool top;
	
	//float dot = new Vector2(Position->x * position->x + Position->y * position->y);
	//std::cout << sgn(dot->x) << ' ' << sgn(dot->y) << '\n';
	left = Position->x > position->x;
	top = Position->y > position->y;
	
	//int rand(rand() % 4);
	//Nodes[rand]->Insert(data, level + 1, position);
	//Insert into correct location
	
	if (left && top) {
		Nodes[0]->Insert(data, level + 1, position);
	}
	else if (!left && top) {
		Nodes[1]->Insert(data, level + 1, position);
	}
	else if (left && !top) {
		Nodes[2]->Insert(data, level + 1, position);
	}
	else {
		Nodes[3]->Insert(data, level + 1, position);
	}

	//delete(diff);
}


//Drawing functionality
void Quadtree::Update() {
	Master->Update();
}

void Quadtree::Node::Update() {
	

	float xMod = ((SCREEN_WIDTH / pow(2, Level )));
	float yMod = ((SCREEN_HEIGHT / pow(2, Level)));

	SDL_Rect rect{ Position->x - xMod / 2, Position->y - yMod / 2, xMod, yMod};
	
	SDL_SetRenderDrawColor(gRenderer, 255, 0, 255, 100);
	SDL_RenderDrawRect(gRenderer, &rect);
	SDL_SetRenderDrawColor(gRenderer, 255, 0, 0, 100);
	pixel(gRenderer, Position->x, Position->y);

	if (Base) return;

	for (int i = 0; i < 4; i++) {
		Nodes[i]->Update();
	}
}