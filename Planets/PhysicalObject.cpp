using namespace std;
#include<PhysicalObject.h>

double Gravitational_Constant = 6.674E-11;
double Drag_Coefficient = 0.999999;

Vector2* PhysicalObject::Bounds = new Vector2();

	//Constructors
	PhysicalObject::PhysicalObject(double mass) {
		Mass = mass;
		Position = new Vector2(0, 0);
		
	}

	PhysicalObject::PhysicalObject(double mass, Vector2* position) {
		Position = position;
		Mass = mass;
		Velocity = new Vector2(0, 0);
	}

	//Destructor
	
	PhysicalObject::~PhysicalObject() {
		delete Position;
		delete Velocity;
	}
	//Gets gravitational force inbetween two objects
	double PhysicalObject::GravitationalForce(PhysicalObject& other) {
		float distance = Vector2::Distance(*Position, *other.Position);
		return (Gravitational_Constant * Mass * other.Mass) / pow((distance > 2) ? distance : 2, 2);
	}

	//Add force to object
	void PhysicalObject::AddForce(double force, Vector2* dir) {
		(Velocity)-> x += dir-> x * force / Mass;
		(Velocity)-> y += dir-> y * force / Mass;
	}

	//Update physics
	void PhysicalObject::Update() {
		if (Static)
			return;
		//Drag
		Velocity->x *= Drag_Coefficient;
		Velocity->y *= Drag_Coefficient;

		//Mass limitations
		if (Mass > 26402814)
			Mass = 26402814;

		//Speed limitations
		if (Velocity->x >= 5)
			Velocity->x = 5;

		if (Velocity->y >= 5)
			Velocity->y = 5;
			
		
		//update positions
		//*Position += (*Velocity / 5.0f);
		
		//Positional clamping
		if (Position->x >= PhysicalObject::Bounds->x) {
			Position->x = -0;
			//Velocity->x = -(Velocity->x) / 25.0f;
		}

		if (Position->x <= 0) {
			Position->x = PhysicalObject::Bounds->x;
			//Velocity->x = -(Velocity->x) / 25.0f;
		}
			
		if (Position->y >= PhysicalObject::Bounds->y) {
			Position->y = 0;
			//Velocity->y = -(Velocity->y) / 50.0f;
		}

		if (Position->y <= 0) {
			Position->y = PhysicalObject::Bounds->y;
			//Velocity->y = -(Velocity->y) / 50.0f;
		}
	}

	//Prints out basic information
	string PhysicalObject::ToString() {
		return string_format("Mass: %e \nPosition:" + Position->ToString() + ")", Mass);
	}

	